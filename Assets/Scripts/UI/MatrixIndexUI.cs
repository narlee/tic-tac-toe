﻿using UnityEngine;
using System;

namespace TicTacToe
{
	/// <summary>
	/// contains of index in UI matrix and get matrix index of UI index
	/// </summary>
	public class MatrixIndexUI : MonoBehaviour
	{
		public MatrixIndex MatrIndex
		{
			get
			{
				return new MatrixIndex()
				{
					Index = Index,
					TypeElement = TypeElement 
				};
			}
		}

		private ElementType TypeElement
		{
			get 
			{
				ElementTypeUI childElType = GetComponentInChildren<ElementTypeUI>();				
				return (childElType != null) ? childElType.Type : ElementType.Empty;
			}
		}

		private Index Index
		{
			get { return new Index(Index_X, Index_Y); }
		}

		public int Index_X;
		public int Index_Y;
	}
}