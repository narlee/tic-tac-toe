﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TicTacToe
{
	/// <summary>
	/// handle mouse clicks and priority of steps between players, 
	/// switch UI elements of ElemetnType
	/// </summary>
	public class SetObjectToPos : MonoBehaviour
	{
		public TicTacToeMatrix matrixUI;
		public ElementTypeUI prefab;		//current prefab ('X' or 'O')
		public About aboutUI;				//about panel
		public ElementTypeUI xUI;			//prefab of 'X'
		public ElementTypeUI oUI;			//prefab of 'O'
		private PvE _pve;

		/// <summary>
		/// switch prefab by ElementType
		/// </summary>
		private void SetUIType(ElementType type)
		{
			switch (type)
			{
				case ElementType.X:
					prefab = xUI;
					break;
				case ElementType.O:
					prefab = oUI;
					break;
			}
		}

		private void Update()
		{
			GameSteps steps = matrixUI
				.GameModel
				.Steps;

			bool block = steps.BlockSteps;
			block |= aboutUI.gameObject.activeSelf | matrixUI.GameModel.Model.CheckForWin();
			if (steps.Mode == GameMode.PvE)
				block |= !matrixUI.GameModel.StepPlayer1;
			if (block)
			{
				_pve.EnemyStep();
				return;
			}
			//catch mouseclick
			RaycastHit hit;
			if (Input.GetKeyDown(KeyCode.Mouse0) &&
				Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
			{
				MatrixIndexUI matrIndex = hit
					.transform
					.GetComponent<MatrixIndexUI>();

				if (matrIndex != null)
				{
					if (steps.CanDoStep(matrIndex.MatrIndex))
						DoStep(matrIndex);
				}
			}
		}

		private void DoStep(MatrixIndexUI matrIndexUI)
		{
			MatrixIndex matrIndex = matrIndexUI.MatrIndex;
			matrIndex.TypeElement = prefab.Type;

			print(String.Format("index:{0}; type:{1}", matrIndex.Index, matrIndex.TypeElement));		//testing
			
			//set element to game model
			matrixUI
				.GameModel
				.Steps
				.DoStep(matrIndex); 
		}

		private void Model_MatrixChanged(object sender, EventArgs<MatrixIndex> e)
		{
			MatrixIndexUI matrIndex = GetTypeUIbyIndex(e.Arg.Index);

			GameObject obj = Instantiate(prefab.gameObject, matrIndex.transform.position, prefab.transform.rotation)
				as GameObject;
			obj.transform.parent = matrIndex.transform;
			SetUIType(matrixUI.GameModel.Steps.StepElType);
		}

		private MatrixIndexUI GetTypeUIbyIndex(Index index)
		{
			return GameObject.FindObjectsOfType<MatrixIndexUI>()
				.Single(e => e.MatrIndex.Index.Equals(index));
		}

		private void Awake()
		{
			SetUIType(matrixUI.GameModel.Model.Player1Element);

			matrixUI.GameModel.Model.MatrixChanged += Model_MatrixChanged;

			_pve = new PvE(matrixUI.GameModel);
		}
	}
}