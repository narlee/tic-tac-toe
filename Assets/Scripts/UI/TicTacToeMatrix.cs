﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace TicTacToe
{
	public class TicTacToeMatrix : MonoBehaviour
	{
		//default positions of Os & Xs
		public Transform pos11;
		public Transform pos12;
		public Transform pos13;
		public Transform pos21;
		public Transform pos22;
		public Transform pos23;
		public Transform pos31;
		public Transform pos32;
		public Transform pos33;
		public Transform winPanel;
		public Text Info;

		public bool WinPanelActive
		{
			get { return winPanel.gameObject.activeSelf; }
		}

		private Text WinText
		{
			get
			{
				var textComponent = winPanel.GetChild(0).GetComponent<Text>();
				return textComponent;
			}
		}

		public TicTacToeGame GameModel { get; set; }

		public void Reset()
		{
			Application.LoadLevel(0);
		}

		public void Exit()
		{
			Application.Quit();
		}

		private void Awake()
		{
			GameModel = new TicTacToeGame();
			GameModel.Model.WinPlayer += Model_WinPlayer;
		}

		private void Update()
		{
			if (!GameModel.Steps.BlockSteps)
			{
				string playerStep = GameModel.StepPlayer1 ? "1" : "2";
				Info.text = String.Format("Game mode: {0};\n player{1}", GameModel.Steps.Mode, playerStep);
			} else
				Info.text = "";

			//show winner panel
			if (Input.GetKeyDown(KeyCode.Mouse0))
			{
				if (WinPanelActive)
				{
					winPanel
						.gameObject
						.SetActive(false);

					Reset();
				}
			}

			if (Input.GetKeyDown(KeyCode.Escape))
				Exit();
		}

		//winner panel (show who is winner)
		private void Model_WinPlayer(object sender, EventArgs<ElementType> e)
		{
			WinText.text = "Winner = " + e.Arg;
			winPanel.gameObject.SetActive(true);
		}
	}
}