﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace TicTacToe
{
	public class About : MonoBehaviour
	{
		const string ABOUT_TEXT =
@"Testproject:	Tic-tac-toe
Developer:	Nazar Zdotovylo
e-mail:			narlee@ukr.net
	Kyyiv, Ukraine
03.2015";

		private void Awake()
		{
			try
			{
				GetComponentInChildren<Text>().text = ABOUT_TEXT;
			} catch { }
		}		

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Mouse0))
				gameObject.SetActive(false);
		}
	}
}