﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace TicTacToe
{
	public class SelectGameMode : MonoBehaviour
	{
		public Button btnPvE_Random;
		public Button btnPvE_Othr;

		public TicTacToeMatrix matrixUI;

		public void StartPvPGame()
		{
			StartGame();
		}

		public void SelectPvEGame()
		{
			btnPvE_Random.gameObject.SetActive(true);
			btnPvE_Othr.gameObject.SetActive(true);
		}

		public void StartPvE_Random()
		{
			matrixUI.GameModel.Steps.Mode = GameMode.PvE;
			StartGame();
		}

		public void StartPvE_Othr()
		{

		}

		public void StartPvE_3()
		{

		}

		private void StartGame()
		{
			gameObject.SetActive(false);

			matrixUI
				.GameModel
				.Steps
				.BlockSteps = false;
		}

		private void Start()
		{
			matrixUI
				.GameModel
				.Steps
				.BlockSteps = true;
		}
	}
}