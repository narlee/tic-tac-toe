﻿using System;

namespace TicTacToe
{
	public class Pair<T1, T2>
	{
		public T1 X { get; set; }
		public T2 Y { get; set; }

		public override bool Equals(object obj)
		{
			if (obj is Pair<T1, T2>)
			{
				Pair<T1, T2> pair = obj as Pair<T1, T2>;
				return this.X.Equals(pair.X) && this.Y.Equals(pair.Y);
			} else
				return base.Equals(obj);
		}

		public static bool operator ==(Pair<T1, T2> pair1, Pair<T1, T2> pair2)
		{
			return pair1.Equals(pair2);
		}
		
		public static bool operator !=(Pair<T1, T2> pair1, Pair<T1, T2> pair2)
		{
			return pair1 != pair2;
		}

		public override string ToString()
		{
			return String.Format("x={0}; y={1}", X, Y);
		}

		public Pair(T1 x, T2 y)
		{
			this.X = x;
			this.Y = y;
		}
	}
}