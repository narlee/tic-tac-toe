﻿using System;

namespace TicTacToe
{
	/// <summary>
	/// elements of tic-tac-toe
	/// </summary>
	public enum ElementType
	{
		Empty,
		X,
		O
	}
}