﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TicTacToe
{
	/// <summary>
	/// matrix of Tic-tac-toe 
	/// include rules of I/O operaions
	/// </summary>
	public class MatrixModel
	{
		public static readonly Index[,] WinWariants = new Index[,]
		{
			//horisontral
			{ new Index(0, 0), new Index(0, 1), new Index(0, 2) },
			{ new Index(1, 0), new Index(1, 1), new Index(1, 2) },
			{ new Index(2, 0), new Index(2, 1), new Index(2, 2) },

			//vertical
			{ new Index(0, 0), new Index(1, 0), new Index(2, 0) },
			{ new Index(0, 1), new Index(1, 1), new Index(2, 1) },
			{ new Index(0, 2), new Index(1, 2), new Index(2, 2) },

			//obliques
			{ new Index(0, 0), new Index(1, 1), new Index(2, 2) },
			{ new Index(0, 2), new Index(1, 1), new Index(2, 0) }
		};

		public event EventHandler<EventArgs<MatrixIndex>> MatrixChanged = (o, e) => { };
		public event EventHandler EndOfGame = (o, e) => { };
		public event EventHandler<EventArgs<ElementType>> WinPlayer = (o, e) => { };

		public ElementType Player1Element
		{
			get { return _player1Element; }
			set
			{
				switch (value)
				{
					case ElementType.X:
					{
						_player1Element = ElementType.X;
						_player2Element = ElementType.O;
						break;
					}
					case ElementType.O:
					{
						_player1Element = ElementType.O;
						_player2Element = ElementType.X;
						break;
					}
				}
			}
		}

		public ElementType Player2Element
		{
			get { return _player2Element; }
		}

		public ElementType[,] Matrix
		{
			get { return _matrix; }
		}

		public IEnumerable<MatrixIndex> MatrixCollection
		{
			get
			{
				List<MatrixIndex> list = new List<MatrixIndex>();
				for (int i = 0; i < _matrix.Length / _matrix.GetLength(0); i++)
				{
					for (int j = 0; j < _matrix.GetLength(1); j++)
					{
						MatrixIndex index = new MatrixIndex(new Index(i, j), _matrix[i, j]);
						list.Add(index);
					}
				}

				return list;
			}
		}

		private ElementType _player1Element = ElementType.X;
		private ElementType _player2Element = ElementType.O;
		private readonly ElementType[,] _matrix = new ElementType[3, 3];

		public IEnumerable<Pair<Index, bool>> MatrixBoolCollection(ElementType selectedEl)
		{
			foreach (var item in MatrixCollection)
				yield return new Pair<Index, bool>(item.Index, item.TypeElement == selectedEl);
		}

		/// <summary>
		/// check is empty index
		/// </summary>
		public bool CheckIndex(MatrixIndex matrIndex)
		{
			ElementType matrixEl = GetElement(matrIndex.Index);	
			return (matrixEl == ElementType.Empty) ? true : false;
		}

		/// <summary>
		/// retrn all empty indexes
		/// </summary>
		/// <returns></returns>
		public IEnumerable<Index> GetFreeCells()
		{
			return MatrixCollection
				.Where(e => e.TypeElement == ElementType.Empty)
				.Select(e => e.Index);
		}

		public bool SetToIndex(MatrixIndex matrIndex)
		{
			if (CheckIndex(matrIndex))
			{
				SetElement(matrIndex);
				return true;
			}

			return false;
		}

		/// <summary>
		/// get ElementType by index
		/// </summary>
		private ElementType GetElement(Index index)
		{
			return Matrix[index.X, index.Y];
		}

		private void SetElement(MatrixIndex matrIindex)
		{
			Matrix[matrIindex.Index.X, matrIindex.Index.Y] = matrIindex.TypeElement;
			CheckForGameEnd();

			MatrixChanged(this, new EventArgs<MatrixIndex>(matrIindex));
		}

		/// <summary>
		/// check for game end
		/// </summary>
		/// <returns>true - if game end</returns>
		private bool CheckForGameEnd()
		{
			if (MatrixCollection.All(e => e.TypeElement != ElementType.Empty) ||
				CheckForWin())
			{
				EndOfGame(this, EventArgs.Empty);

				ElementType? winnerRes = GetWinner();
				ElementType setWinValue = ElementType.Empty;
				if (winnerRes.HasValue)
					setWinValue = winnerRes.Value;

				EventArgs<ElementType> eventArg = new EventArgs<ElementType>(setWinValue);
				WinPlayer(this, eventArg);
				return true;
			}

			return false;
		}

		public bool CheckForWin()
		{
			ElementType? winnerRes = GetWinner();

			if (winnerRes.HasValue)
			{
				EventArgs<ElementType> eventArg = new EventArgs<ElementType>(GetWinner().Value);
				WinPlayer(this, eventArg);
				return true;
			}
			
			return false;
		}

		/// <summary>
		/// get winner of the game
		/// </summary>
		/// <returns>null - if game not ended</returns>
		public ElementType? GetWinner()
		{
			for (int i = 0; i < WinWariants.GetLength(0); i++)
			{
				int ko = 0, kx = 0;
				for (int j = 0; j < WinWariants.GetLength(1); j++)
				{
					foreach (var item in MatrixCollection)
					{
						if (item.Index == WinWariants[i, j])
						{
							if (item.TypeElement == ElementType.O)
								ko++;
							if (item.TypeElement == ElementType.X)
								kx++;
						}

						if (ko == 3)
							return ElementType.O;
						if (kx == 3)
							return ElementType.X;
					}
				}
			}

			return null;
		}
	}
}