﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TicTacToe
{
	public class PvE
	{
		public GameMode Mode { get; set; }

		private TicTacToeGame _game;

		public void EnemyStep()
		{
			if (_game.Steps.Mode != GameMode.PvE ||
				_game.Steps.BlockSteps ||
				_game.Model.CheckForWin())
				return;

			switch (Mode)
			{
				case GameMode.PvE_Random:
				{
					RandomStep();
					break;
				}
			}
		}

		private void DoStep(MatrixIndex index)
		{
			_game.Steps.DoStep(index);
		}

		private void RandomStep()
		{
			Index[] freeIndex = _game
				.Model
				.GetFreeCells()
				.ToArray();
			if (freeIndex.Length < 1)
				return;

			Random rnd = new Random();
			int next = rnd.Next(0, freeIndex.Length - 1);		//need to test
			UnityEngine.Debug.Log(next);

			MatrixIndex ind = new MatrixIndex(freeIndex[next], _game.Model.Player2Element);
			DoStep(ind);
		}

		private void OthrStep()
		{
			throw new NotImplementedException();
		}

		public PvE(TicTacToeGame game)
		{
			this.Mode = GameMode.PvE_Random;
			this._game = game;
		}
	}
}