﻿using System;

namespace TicTacToe
{
	/// <summary>
	/// index of multid. array
	/// </summary>
	public class Index : Pair<int, int>
	{
		public Index(int x, int y)
			: base(x, y)
		{
			if (x < 0 || y < 0)
				throw new ArgumentOutOfRangeException();
		}
	}
}