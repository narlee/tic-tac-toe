﻿using System;

namespace TicTacToe
{
	/// <summary>
	/// index of multid. array
	/// </summary>
		//public
	struct Index_Old
	{
		public int X { get; set; }
		public int Y { get; set; }

		public override string ToString()
		{
			return String.Format("x={0}; y={1}", X, Y);
		}

		public Index_Old(int x, int y)
			: this()
		{
			if (x < 0 || y < 0)
				throw new ArgumentOutOfRangeException();

			this.X = x;
			this.Y = y;
		}
	}
}