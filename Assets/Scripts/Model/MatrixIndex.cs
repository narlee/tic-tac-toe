﻿using UnityEngine;
using System;

namespace TicTacToe
{
	/// <summary>
	/// contains matrix index [i, j] and type of this index
	/// </summary>
	public class MatrixIndex
	{
		public ElementType TypeElement { get; set; }
		public Index Index { get; set; }

		public override string ToString()
		{
			return String.Format("{0}; {1}", Index, TypeElement);
		}

		public MatrixIndex() { }

		public MatrixIndex(Index index, ElementType typeElement)
		{
			this.Index = index;
			this.TypeElement = typeElement;
		}
	}
}