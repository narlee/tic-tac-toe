﻿	using System;
using System.Collections.Generic;

namespace TicTacToe
{
	public class GameSteps
	{
		public event EventHandler<EventArgs<ElementType>> ElementTypeChanged = (o, e) => { };

		public GameMode	Mode { get; set; }

		/// <summary>
		/// ElementType of current step
		/// </summary>
		public ElementType StepElType
		{
			get { return _type; }
			set
			{
				_type = value;
				ElementTypeChanged(this, new EventArgs<ElementType>(value));
			}
		}
		
		/// <summary>
		/// can doing step any player
		/// </summary>
		public bool BlockSteps { get; set; }

		private TicTacToeGame _game;
		private ElementType _type;

		/// <summary>
		/// check if current player can do step
		/// </summary>
		/// <returns>true - if can do step, else - can't</returns>
		public bool CanDoStep(MatrixIndex matrIndex)
		{
			if (BlockSteps)
				return false;

			return (matrIndex != null) ? _game.Model.CheckIndex(matrIndex) : false;
		}

		public void DoStep(MatrixIndex matrIndex)
		{
			if (!CanDoStep(matrIndex))
				return;

			//if (matrIndex != null)
			bool res = _game.Model.SetToIndex(matrIndex);
		}

		public GameSteps(TicTacToeGame game, bool BlockSteps = false)
		{
			this.Mode = GameMode.PvP;
			this._game = game;
			this.BlockSteps = BlockSteps;
		}
	}
}