﻿using System;

namespace System.Collections.Generic
{
	public class EventArgs<T> : EventArgs
	{
		public T Arg
		{
			get { return _arg; }
		}

		private readonly T _arg;

		public EventArgs(T arg)
			: base()
		{
			_arg = arg;
		}
	}
}