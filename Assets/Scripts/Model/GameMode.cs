﻿using System;

namespace TicTacToe
{
	public enum GameMode
	{
		PvP,
		PvE,
		PvE_Random,
		PvE_Othr
		//PvE_3
		//...
	}
}