﻿using System;
using System.Collections.Generic;

namespace TicTacToe
{
	/// <summary>
	/// full model of the Tic-Tac-Toe game
	/// </summary>
	public class TicTacToeGame
	{
		public MatrixModel Model { get; private set; }
		public GameSteps Steps { get; private set; }

		/// <summary>
		/// if true - player1 can do 1 step
		/// </summary>
		public bool StepPlayer1 { get; set; }

		public TicTacToeGame()
		{
			StepPlayer1 = true;
			Steps = new GameSteps(this);

			Model = new MatrixModel();
			Model.MatrixChanged += Model_MatrixChanged;
		}

		private void Model_MatrixChanged(object sender, EventArgs<MatrixIndex> e)
		{
			StepPlayer1 = !StepPlayer1;
			Steps.StepElType = StepPlayer1 ? ElementType.X : ElementType.O;
		}
	}
}